import { Meteor } from 'meteor/meteor';
import { Router, Switch, Route, Redirect } from 'react-router';

import React from 'react';
import Signup from '../ui/Signup';
import Dashboard from '../ui/Dashboard';
import NotFound from '../ui/NotFound';
import Login from '../ui/Login';
import { history } from '../libraries/history';

const unauthenticatedPages = ['/', '/signup'];
const authenticatedPages = ['/dashboard'];

const onEnterPublicPage = Component => {
    if (Meteor.userId()) {
        return <Redirect to="/dashboard" />;
    }
    return <Component />;
};

const onEnterPrivatePage = Component => {
    if (Meteor.userId()) {
        return <Component />;
    }
    return <Redirect to="/" />;
};

export const routes = (
    <Router history={history}>
        <Switch>
            <Route exact path="/" render={() => onEnterPublicPage(Login)} />
            <Route path="/signup" render={() => onEnterPublicPage(Signup)} />
            <Route
                path="/dashboard"
                render={() => onEnterPrivatePage(Dashboard)}
            />
            <Route path="*" component={NotFound} />
        </Switch>
    </Router>
);

export const onAuthChange = isAuthenticated => {
    const pathname = history.location.pathname;
    const isUnauthenticatedPage = unauthenticatedPages.includes(pathname);
    const isAuthenticatedPage = authenticatedPages.includes(pathname);

    if (isUnauthenticatedPage && isAuthenticated) {
        history.push('/dashboard');
    }
    if (isAuthenticatedPage && !isAuthenticated) {
        history.push('/');
    }
};
